package conversores.impl;

import conversores.GenericConverter;
import excepciones.PersonaException;
import objetos.dto.PersonaDTO;
import objetos.entidades.Persona;

public class PersonaConverter implements GenericConverter<Persona, PersonaDTO> {

	public PersonaConverter() {
		// TODO Auto-generated constructor stub
	}
	
	public PersonaDTO entityToDTO(Persona persona) throws PersonaException {
		
		if(persona == null) {
			throw new PersonaException(PersonaException.PERSONA_NULA);
		} else if(persona.getNombre() == null || persona.getApe1()== null || persona.getApe2()== null) {
			throw new PersonaException(PersonaException.DATOS_NULOS);
		}
		
		PersonaDTO dto = new PersonaDTO();
		dto.setNombre(persona.getNombre());
		dto.setApe1(persona.getApe1());
		dto.setApe2(persona.getApe2());
		
		return dto;
	}

	public Persona DTOToEntity(PersonaDTO dto) throws PersonaException {
		
		if(dto == null) {
			throw new PersonaException(PersonaException.PERSONA_NULA);
		} else if(dto.getNombre() == null || dto.getApe1()== null || dto.getApe2()== null) {
			throw new PersonaException(PersonaException.DATOS_NULOS);
		}
		
		Persona persona = new Persona();
		persona.setNombre(dto.getNombre());
		persona.setApe1(dto.getApe1());
		persona.setApe2(dto.getApe2());
		return persona;
	}

}
