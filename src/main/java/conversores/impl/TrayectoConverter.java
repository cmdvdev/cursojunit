package conversores.impl;

import conversores.GenericConverter;
import excepciones.TrayectoException;
import objetos.dto.TrayectoDTO;
import objetos.entidades.Trayecto;

public class TrayectoConverter implements GenericConverter<Trayecto, TrayectoDTO>{

	public TrayectoDTO entityToDTO(Trayecto t) throws TrayectoException {
		
		if(t == null) {
			throw new TrayectoException(TrayectoException.TRAYECTO_NULO);		
		}
		
		if(t.getDestino() == null || t.getDestino().isEmpty()
				|| t.getOrigen() == null || t.getOrigen().isEmpty()) {
			throw new TrayectoException(TrayectoException.ORIGEN_O_DESTINO_NULO);
		}
		
		TrayectoDTO dto = new TrayectoDTO();
		dto.setDestino(t.getDestino());
		dto.setOrigen(t.getOrigen());
		return dto;
	}

	public Trayecto DTOToEntity(TrayectoDTO dto) throws TrayectoException {
		
		if(dto == null) {
			throw new TrayectoException(TrayectoException.TRAYECTO_NULO);		
		}
		
		if(dto.getDestino() == null || dto.getDestino().isEmpty()
				|| dto.getOrigen() == null || dto.getOrigen().isEmpty()) {
			throw new TrayectoException(TrayectoException.ORIGEN_O_DESTINO_NULO);
		}
		
		Trayecto t = new Trayecto();
		t.setOrigen(dto.getOrigen());
		t.setDestino(dto.getDestino());
		return t;
	}

}
