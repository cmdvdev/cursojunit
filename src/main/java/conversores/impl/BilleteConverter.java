package conversores.impl;

import conversores.GenericConverter;
import excepciones.BilleteExeption;
import excepciones.PersonaException;
import excepciones.TrayectoException;
import objetos.dto.BilleteDTO;
import objetos.entidades.Billete;

public class BilleteConverter implements GenericConverter<Billete, BilleteDTO> {

	private TrayectoConverter trayectoConverter;
	private PersonaConverter personaConverter;
	
	public BilleteConverter() {
		this(new TrayectoConverter(), new PersonaConverter());
	}
	
	public BilleteConverter(TrayectoConverter trayectoConverter, PersonaConverter personaConverter) {
		super();
		this.trayectoConverter = trayectoConverter;
		this.personaConverter = personaConverter;
	}
		
	public BilleteDTO entityToDTO(Billete billete) throws TrayectoException, PersonaException, BilleteExeption {
		BilleteDTO dto = new BilleteDTO();
		dto.setTrayecto(trayectoConverter.entityToDTO(billete.getTrayecto()));
		dto.setViajero(personaConverter.entityToDTO(billete.getViajero()));
		
		if(dto.getPrecio() == null || dto.getPrecio() < 0) {
			throw new BilleteExeption(BilleteExeption.INPORTE_INVALIDO);
		}
		dto.setPrecio(billete.getPrecio());
		return dto;
	}

	public Billete DTOToEntity(BilleteDTO dto) throws PersonaException, TrayectoException, BilleteExeption {
		Billete billete = new Billete();
		billete.setTrayecto(trayectoConverter.DTOToEntity(dto.getTrayecto()));
		billete.setViajero(personaConverter.DTOToEntity(dto.getViajero()));
		
		if(dto.getPrecio() == null || dto.getPrecio() < 0) {
			throw new BilleteExeption(BilleteExeption.INPORTE_INVALIDO);
		}
		billete.setPrecio(dto.getPrecio());
		return billete;
	}

}
