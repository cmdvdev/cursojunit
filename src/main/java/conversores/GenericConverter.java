package conversores;

public interface GenericConverter<T,S> {

	S entityToDTO(T t) throws Exception;
	
	T DTOToEntity(S s) throws Exception;
}
