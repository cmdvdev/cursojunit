package main;

import javax.net.ssl.SSLEngineResult.Status;

import excepciones.BilleteExeption;
import excepciones.PersonaException;
import excepciones.TrayectoException;
import objetos.dto.BilleteDTO;
import objetos.dto.PersonaDTO;
import objetos.dto.TrayectoDTO;
import servicio.GestionBilletes;
import servicio.impl.GestionBilletesServiceImpl;

public class Main {

	private static final int EXIT_WITH_ERROR = 1;
	private GestionBilletes servicio;

	public static void main(String... args) {
		System.out.println("-- INICIO --");
		Main main = new Main();
		main.init();
	}

	public void init() {
		servicio = new GestionBilletesServiceImpl();

		PersonaDTO viajero = new PersonaDTO("Juan", "Lopez", "Perez");
		TrayectoDTO trayecto = new TrayectoDTO("BUE", "MAD");
		Double precio = -950d;
		BilleteDTO billeteDTO = new BilleteDTO(viajero, trayecto, precio);

		Integer idBilleteARecuperar = 1;

		almacenaBillete(billeteDTO);
		recuperaBillete(idBilleteARecuperar);
	}

	private void almacenaBillete(BilleteDTO billeteDTO) {
		try {
			servicio.addBillete(billeteDTO);
		} catch (TrayectoException e) {
			System.out.println("No se puede guardar el trayecto" + e.getMessage());
		} catch (PersonaException e) {
			System.out.println("Error en los datos de la persona. " + e.getMessage());
		} catch (BilleteExeption e) {
			System.out.println("No se ha podido guardar el billete" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Excepci�n no soportada!!!");
			e.printStackTrace();
		} finally {
			System.exit(EXIT_WITH_ERROR);
		}
	}

	private void recuperaBillete(Integer idBillete) {
		BilleteDTO recuperado = null;
		try {
			recuperado = servicio.getBillete(idBillete);
		} catch (TrayectoException e) {
			System.out.println("No se ha podido recuperar el billete" + e.getMessage());
		} catch (PersonaException e) {
			System.out.println("Error en los datos de la persona. " + e.getMessage());
		} catch (BilleteExeption e) {
			System.out.println("No se ha podido recuperar el billete" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Excepci�n no soportada!!!");
			e.printStackTrace();
		} finally {
			System.exit(EXIT_WITH_ERROR);
		}

		System.out.println("Recuperado, origen: " + recuperado.getTrayecto().getOrigen());
		System.out.println("Recuperado, destino: " + recuperado.getTrayecto().getDestino());
		System.out.println("Viajero, nombre: " + recuperado.getViajero().getNombre());
		System.out.println("Viajero, ape1: " + recuperado.getViajero().getApe1());
		System.out.println("-- FIN --");
	}

}
