package objetos.entidades;

public class Billete {

	private Persona viajero;
	private Trayecto trayecto;
	private Double precio;
	
	public Billete() {
		// TODO Auto-generated constructor stub
	}

	public Billete(Persona viajero, Trayecto trayecto, Double precio) {
		super();
		this.viajero = viajero;
		this.trayecto = trayecto;
		this.precio = precio;
	}
		
	public Persona getViajero() {
		return viajero;
	}

	public void setViajero(Persona viajero) {
		this.viajero = viajero;
	}

	public Trayecto getTrayecto() {
		return trayecto;
	}

	public void setTrayecto(Trayecto trayecto) {
		this.trayecto = trayecto;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}




	
}
