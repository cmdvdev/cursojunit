package objetos.entidades;

public class Trayecto {

	private String origen;
	private String destino;
	
	public Trayecto() {
		// TODO Auto-generated constructor stub
	}
	
	public Trayecto(String origen, String destino) {
		super();
		this.origen = origen;
		this.destino = destino;
	}
	
	public String getOrigen() {
		return origen;
	}
	
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	
}
