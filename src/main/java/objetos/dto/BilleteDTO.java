package objetos.dto;

public class BilleteDTO {

	private PersonaDTO viajero;
	private TrayectoDTO trayecto;
	private Double precio;
	
	public BilleteDTO() {
		// TODO Auto-generated constructor stub
	}

	public BilleteDTO(PersonaDTO viajero, TrayectoDTO trayecto, Double precio) {
		super();
		this.viajero = viajero;
		this.trayecto = trayecto;
		this.precio = precio;
	}
		
	public PersonaDTO getViajero() {
		return viajero;
	}

	public void setViajero(PersonaDTO viajero) {
		this.viajero = viajero;
	}

	public TrayectoDTO getTrayecto() {
		return trayecto;
	}

	public void setTrayecto(TrayectoDTO trayecto) {
		this.trayecto = trayecto;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}




	
}
