package servicio;

import objetos.dto.BilleteDTO;

public interface GestionBilletes {

	void addBillete(BilleteDTO billeteDTO) throws Exception;
	
	BilleteDTO getBillete(Integer num) throws Exception;
}
