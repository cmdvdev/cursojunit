package servicio.impl;

import conversores.impl.BilleteConverter;
import excepciones.BilleteExeption;
import excepciones.PersonaException;
import excepciones.TrayectoException;
import objetos.dto.BilleteDTO;
import objetos.entidades.Billete;
import repositorio.BilletesDAO;
import repositorio.impl.BilletesDAOImpl;
import servicio.GestionBilletes;

public class GestionBilletesServiceImpl implements GestionBilletes {

	private BilletesDAO billetesDAO = new BilletesDAOImpl();
	private BilleteConverter billeteConverter = new BilleteConverter();
	
	public void addBillete(BilleteDTO billeteDTO) throws PersonaException, TrayectoException, BilleteExeption {
		System.out.println("-- Servicio addBillete --");
		billetesDAO.addBillete(billeteConverter.DTOToEntity(billeteDTO));	
	}

	public BilleteDTO getBillete(Integer num) throws TrayectoException, PersonaException, BilleteExeption {
		System.out.println("-- Servicio getBillete --");
		Billete billete = billetesDAO.getBillete(num);	
		return billeteConverter.entityToDTO(billete);
	}
}
