package excepciones;

public class BilleteExeption extends Exception {

	private static final long serialVersionUID = -2551550485174611402L;
	public static final String INPORTE_INVALIDO = "Error: El importe no puede ser negativo o nulo";

	public BilleteExeption(String msg) {
		 super(msg);
	}
}