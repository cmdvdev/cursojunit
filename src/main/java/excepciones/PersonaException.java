package excepciones;

public class PersonaException extends Exception {

	private static final long serialVersionUID = 1L;
	public static final String PERSONA_NULA = "Error: persona nula";
	public static final String DATOS_NULOS = "Error: Alg�n dato de la persona es nulo";

	 public PersonaException(String msg) {
	        super(msg);
	    }
}
