package excepciones;

public class TrayectoException extends Exception {

	private static final long serialVersionUID = 1L;
	public static final String TRAYECTO_NULO = "Error: Trayecto nulo";
	public static final String ORIGEN_O_DESTINO_NULO = "Error: Origen o destino incorrecto";

	 public TrayectoException(String msg) {
	        super(msg);
	    }
}
