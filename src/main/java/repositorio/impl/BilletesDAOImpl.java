package repositorio.impl;

import java.util.HashMap;
import java.util.Map;

import objetos.entidades.Billete;
import repositorio.BilletesDAO;

public class BilletesDAOImpl implements BilletesDAO {

	Map<Integer, Billete> billeteMap = new HashMap<Integer, Billete>();
	public static Integer contador = 1;
	
	public void addBillete(Billete billete) {
			
		System.out.println("-- DAO add billete --");
		billeteMap.put(contador, billete);
		contador++;
		
		System.out.println("-- El tama�o del mapa es " + billeteMap.size() + " --");
		System.out.println("-- El valor actual del contador es "+ contador +" --");
	}

	public Billete getBillete(Integer num) {
		System.out.println("-- DAO add billete --");
		if(billeteMap.containsKey(num)) {
			return billeteMap.get(num);
		}
		return null;
	}

}
