package repositorio;

import objetos.entidades.Billete;

public interface BilletesDAO {
	
	void addBillete(Billete billete);
	
	Billete getBillete(Integer num);
}
