package conversores;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import conversores.impl.BilleteConverter;
import conversores.impl.PersonaConverter;
import conversores.impl.TrayectoConverter;
import excepciones.BilleteExeption;
import excepciones.PersonaException;
import excepciones.TrayectoException;
import objetos.dto.BilleteDTO;
import objetos.dto.PersonaDTO;
import objetos.dto.TrayectoDTO;
import objetos.entidades.Billete;
import objetos.entidades.Persona;

public class BilleteConverterIntegrationTest {
	
	private static final double PRECIO_BILLETE = 15d;

	private PersonaConverter personaConverter = new PersonaConverter();

	private TrayectoConverter trayectoConverter = new TrayectoConverter();
	
	private BilleteConverter billeteConverter = new BilleteConverter(trayectoConverter, personaConverter);

	private TrayectoDTO trayectoDTO;
	private PersonaDTO personaDTO;
	
	@Before
	public void setUp() throws Exception {
		trayectoDTO = new TrayectoDTO("VLC", "MAD");		
		personaDTO = new PersonaDTO("nombre", "ape1", "ape2");
	}

	@Test
	public void conversionDeEntidadADTOTest() throws PersonaException, TrayectoException, BilleteExeption {
		BilleteDTO billeteDTO = new BilleteDTO(personaDTO , trayectoDTO, PRECIO_BILLETE);
		
		Billete billete = billeteConverter.DTOToEntity(billeteDTO);
		
		Assert.assertThat(billete, Matchers.notNullValue());
		Assert.assertThat(billete, Matchers.instanceOf(Billete.class));
		Assert.assertThat(billete.getTrayecto(), Matchers.notNullValue());
		Assert.assertThat(billete.getTrayecto().getOrigen(), Matchers.is("VLC"));
		Assert.assertThat(billete.getTrayecto().getDestino(), Matchers.is("MAD"));
		Assert.assertThat(billete.getViajero(), Matchers.notNullValue());
		Assert.assertThat(billete.getViajero(), Matchers.instanceOf(Persona.class));
		Assert.assertThat(billete.getViajero().getNombre(), Matchers.is("nombre"));
		Assert.assertThat(billete.getViajero().getApe1(), Matchers.is("ape1"));
		Assert.assertThat(billete.getViajero().getApe2(), Matchers.is("ape2"));
		Assert.assertThat(billete.getPrecio(), Matchers.is(PRECIO_BILLETE));
	}
}
