package conversores.impl;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import excepciones.TrayectoException;
import objetos.dto.TrayectoDTO;
import objetos.entidades.Trayecto;

public class TrayectoConverterTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	private static final String DESTINO = "destino";
	private static final String ORIGEN = "origen";
	private static final String EMPTY_STRING = "";
	
	private TrayectoConverter converter;
	
	@Before
	public void setUp() throws Exception {
		converter = new TrayectoConverter();
	}

	@Test
	public void convertEntityToDTOTest() throws TrayectoException {
		// given
		Trayecto trayecto = new Trayecto(ORIGEN, DESTINO);
		
		//when
		TrayectoDTO trayectoDTO = converter.entityToDTO(trayecto);
		
		//then
		Assert.assertThat(trayectoDTO, Matchers.instanceOf(TrayectoDTO.class));
		Assert.assertThat(trayectoDTO, Matchers.notNullValue());
		Assert.assertThat(trayectoDTO.getOrigen(), Matchers.notNullValue());
		Assert.assertThat(trayectoDTO.getDestino(), Matchers.notNullValue());
		Assert.assertThat(trayectoDTO.getOrigen(), Matchers.not(Matchers.isEmptyString()));
		Assert.assertThat(trayectoDTO.getDestino(), Matchers.not(Matchers.isEmptyString()));
		Assert.assertThat(trayectoDTO.getOrigen(), Matchers.is(ORIGEN));
		Assert.assertThat(trayectoDTO.getDestino(), Matchers.is(DESTINO));
	}
	
	@Test
	public void siOrigenNulosOVacioLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoOrigen = new Trayecto(null, DESTINO);
		
		//when
		converter.entityToDTO(trayectoNoOrigen);
	}
	
	@Test
	public void siDestinoNuloOVacioLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoDestino = new Trayecto(ORIGEN, null);
		
		//when
		converter.entityToDTO(trayectoNoDestino);
	}
	
	@Test
	public void siOrigenYDestinoNulosOVacioLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoOrigenNiDestino = new Trayecto(null, null);
		
		//when
		converter.entityToDTO(trayectoNoOrigenNiDestino);
	}

	@Test
	public void siOrigenVacioLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoOrigen = new Trayecto(EMPTY_STRING, DESTINO);
		
		//when
		converter.entityToDTO(trayectoNoOrigen);
	}
	
	@Test
	public void siDestinoVacioLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoDestino = new Trayecto(ORIGEN, EMPTY_STRING);
		
		//when
		converter.entityToDTO(trayectoNoDestino);
	}
	
	@Test
	public void siOrigenYDestinoVaciosLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.ORIGEN_O_DESTINO_NULO);
		
		// given
		Trayecto trayectoNoOrigenNiDestino = new Trayecto(EMPTY_STRING, EMPTY_STRING);
		
		//when
		converter.entityToDTO(trayectoNoOrigenNiDestino);
	}
	
	@Test
	public void siTrayectoNuloLanzaExcepcion() throws TrayectoException {
		
		exception.expect(TrayectoException.class);
		exception.expectMessage(TrayectoException.TRAYECTO_NULO);
		
		//when
		converter.entityToDTO(null);
	}
}
