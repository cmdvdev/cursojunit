package conversores.impl;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import conversores.impl.BilleteConverter;
import excepciones.BilleteExeption;
import excepciones.PersonaException;
import excepciones.TrayectoException;
import objetos.dto.BilleteDTO;
import objetos.dto.PersonaDTO;
import objetos.dto.TrayectoDTO;
import objetos.entidades.Billete;
import objetos.entidades.Persona;
import objetos.entidades.Trayecto;

@RunWith(MockitoJUnitRunner.class)
public class BilleteConverterTest {
	
	private static final double PRECIO_BILLETE = 15d;

	@Mock
	private PersonaConverter personaConverter;

	@Mock
	private TrayectoConverter trayectoConverter;
	
	@InjectMocks
	private BilleteConverter billeteConverter;

	private Trayecto trayecto;
	private TrayectoDTO trayectoDTO;
	private Persona personaMock;
	
	@Before
	public void setUp() throws Exception {
		// Con un objeto real
		trayecto = new Trayecto("VLC", "MAD");
		trayectoDTO = new TrayectoDTO("VLC", "MAD");
		Mockito.when(trayectoConverter.DTOToEntity(trayectoDTO)).thenReturn(trayecto);
		
		// Con un mock
		personaMock = Mockito.mock(Persona.class);
		Mockito.when(personaConverter.DTOToEntity(Mockito.any(PersonaDTO.class))).thenReturn(personaMock);
		
	}

	@Test
	public void conversionDeEntidadADTOTest() throws PersonaException, TrayectoException, BilleteExeption {
		PersonaDTO persona = new PersonaDTO("nombre", "ape1", "ape2");
		BilleteDTO billeteDTO = new BilleteDTO(persona , trayectoDTO, PRECIO_BILLETE);
		
		Billete billete = billeteConverter.DTOToEntity(billeteDTO);
		
		Assert.assertThat(billete, Matchers.notNullValue());
		Assert.assertThat(billete, Matchers.instanceOf(Billete.class));
		Assert.assertThat(billete.getTrayecto(), Matchers.notNullValue());
		Assert.assertThat(billete.getTrayecto().getOrigen(), Matchers.is("VLC"));
		Assert.assertThat(billete.getTrayecto().getDestino(), Matchers.is("MAD"));
		Assert.assertThat(billete.getViajero(), Matchers.notNullValue());
		Assert.assertThat(billete.getViajero(), Matchers.instanceOf(Persona.class));
		Assert.assertThat(billete.getPrecio(), Matchers.is(PRECIO_BILLETE));
	}
}
