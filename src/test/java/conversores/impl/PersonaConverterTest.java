package conversores.impl;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import objetos.dto.PersonaDTO;
import objetos.entidades.Persona;

public class PersonaConverterTest {

	private PersonaConverter converter;
	
	@Before
	public void setUp(){
		converter = new PersonaConverter();
	}

	@Test
	public void testEntityToDTO() throws Exception {
		//given
		Persona persona = new Persona("nombre", "ape1", "ape2");

		//when
		PersonaDTO personaDTO = converter.entityToDTO(persona);

		//then
		Assert.assertThat(personaDTO, Matchers.notNullValue());
		Assert.assertThat(personaDTO, Matchers.isA(PersonaDTO.class));
		Assert.assertThat(personaDTO.getNombre(), Matchers.is(persona.getNombre()));
		Assert.assertThat(personaDTO.getApe1(), Matchers.is(persona.getApe1()));
		Assert.assertThat(personaDTO.getApe2(), Matchers.is(persona.getApe2()));
	}
	
	@Test
	public void siPersonaEsNuloPersonaDTOEsNuloTest() throws Exception {
		//given
		Persona persona = null;

		//when
		PersonaDTO personaDTO = converter.entityToDTO(persona);

		//then
		Assert.assertThat(personaDTO, Matchers.nullValue());
	}
	
	@Test
	public void siAtributoEsNuloEnDTOTest() throws Exception {
		//given
		Persona persona = new Persona("nombre", null, "");

		//when
		PersonaDTO personaDTO = converter.entityToDTO(persona);

		//then
		Assert.assertThat(personaDTO, Matchers.notNullValue());
		Assert.assertThat(personaDTO, Matchers.isA(PersonaDTO.class));
		Assert.assertThat(personaDTO.getNombre(), Matchers.is(persona.getNombre()));
		Assert.assertThat(personaDTO.getApe1(), Matchers.nullValue());
		Assert.assertThat(personaDTO.getApe2(), Matchers.isEmptyString());
	}


	@Test
	public void testDTOToEntity() throws Exception {
		//given
		PersonaDTO personaDTO = new PersonaDTO("nombre", "ape1", "ape2");

		//when
		Persona persona = converter.DTOToEntity(personaDTO);

		//then
		Assert.assertThat(persona, Matchers.notNullValue());
		Assert.assertThat(persona, Matchers.isA(Persona.class));
		Assert.assertThat(persona.getNombre(), Matchers.is(personaDTO.getNombre()));
		Assert.assertThat(persona.getApe1(), Matchers.is(personaDTO.getApe1()));
		Assert.assertThat(persona.getApe2(), Matchers.is(personaDTO.getApe2()));
	}

	@Test
	public void siPersonaDTOEsNuloPersonaEsNuloTest() throws Exception {
		//given
		PersonaDTO personaDTO = null;

		//when
		Persona persona= converter.DTOToEntity(personaDTO);

		//then
		Assert.assertThat(persona, Matchers.nullValue());
	}

	@Test
	public void siAtributoEsNuloEnEntidadTest() throws Exception {
		//given
		PersonaDTO personaDTO = new PersonaDTO("nombre", null, "");

		//when
		Persona persona= converter.DTOToEntity(personaDTO);

		//then
		Assert.assertThat(persona, Matchers.notNullValue());
		Assert.assertThat(persona, Matchers.isA(Persona.class));
		Assert.assertThat(persona.getNombre(), Matchers.is(personaDTO.getNombre()));
		Assert.assertThat(persona.getApe1(), Matchers.nullValue());
		Assert.assertThat(persona.getApe2(), Matchers.isEmptyString());
	}

}
